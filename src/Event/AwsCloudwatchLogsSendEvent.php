<?php

namespace Drupal\aws_cloudwatchlogs\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is triggered on when logs are sent to AWS Cloudwatch.
 */
class AwsCloudwatchLogsSendEvent extends Event {

  const SEND_LOGS = 'aws_cloudwatchlogs.send_logs';

  /**
   * The log message.
   *
   * @var string
   */
  protected $message;

  /**
   * The log group.
   *
   * @var string
   */
  protected $logGroup;

  /**
   * The log stream.
   *
   * @var string
   */
  protected $logStream;

  /**
   * Associative array providing context.
   *
   * @var array
   */
  protected $context;

  /**
   * Event constructor.
   *
   * @param string $message
   *   The log message.
   * @param string $log_group
   *   The log group.
   * @param string $log_stream
   *   The log stream.
   * @param array $context
   *   Associative array providing context.
   */
  public function __construct($message, $log_group, $log_stream, array $context) {
    $this->message = $message;
    $this->logGroup = $log_group;
    $this->logStream = $log_stream;
    $this->context = $context;
  }

  /**
   * Get context of message log.
   *
   * @return array
   *   The context message log.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Set context of message log.
   *
   * @param array $context
   *   The context message log.
   */
  public function setContext(array $context) {
    $this->context = $context;
  }

  /**
   * Get the message of log.
   *
   * @return string
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Set the message of log.
   *
   * @param string $message
   *   The log message.
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * Get the name of log group to send in aws.
   *
   * @return string
   */
  public function getLogGroup() {
    return $this->logGroup;
  }

  /**
   * Set the log group.
   *
   * @param string $log_group
   *   The log group.
   */
  public function setLogGroup($log_group) {
    $this->logGroup = $log_group;
  }

  /**
   * Get the name of log stream to send in aws.
   *
   * @return string
   */
  public function getLogStream() {
    return $this->logStream;
  }

  /**
   * Set the log stream.
   *
   * @param string $log_stream
   *   The log stream.
   */
  public function setLogStream($log_stream) {
    $this->logStream = $log_stream;
  }

}
