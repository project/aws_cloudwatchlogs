<?php

namespace Drupal\aws_cloudwatchlogs\Logger;

use Drupal\aws_cloudwatchlogs\Event\AwsCloudwatchLogsSendEvent;
use Drupal\aws_cloudwatchlogs\Form\AwsCloudwatchLogSettingsForm;
use Drupal\aws_cloudwatchlogs\Services\DescribeLogGroups;
use Drupal\aws_cloudwatchlogs\Services\DescribeLogStreams;
use Drupal\aws_cloudwatchlogs\Services\PutLogEvents;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Logs events in AWS Cloudwatch.
 */
class Log implements LoggerInterface {

  use RfcLoggerTrait;

  const LOG_LEVELS = [
    RfcLogLevel::EMERGENCY => LogLevel::EMERGENCY,
    RfcLogLevel::ALERT => LogLevel::ALERT,
    RfcLogLevel::CRITICAL => LogLevel::CRITICAL,
    RfcLogLevel::ERROR => LogLevel::ERROR,
    RfcLogLevel::WARNING => LogLevel::WARNING,
    RfcLogLevel::NOTICE => LogLevel::NOTICE,
    RfcLogLevel::INFO => LogLevel::INFO,
    RfcLogLevel::DEBUG => LogLevel::DEBUG,
  ];

  const DEFAULT_FMT = '[@severity] [@type] [@date] @message | @uid | @request_uri | @referer | @ip | @link';

  /**
   * The AWS service to put log inside cloudwatch.
   *
   * @var \Drupal\aws_cloudwatchlogs\Services\PutLogEvents
   */
  protected $putLog;

  /**
   * The service to get log group of Cloudwatch.
   *
   * @var \Drupal\aws_cloudwatchlogs\Services\DescribeLogGroups
   */
  protected $logGroups;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The service to get log stream of Cloudwatch.
   *
   * @var \Drupal\aws_cloudwatchlogs\Services\DescribeLogStreams
   */
  protected $logStreams;

  /**
   * Constructs a Log object.
   *
   * @param \Drupal\aws_cloudwatchlogs\Services\PutLogEvents $put_log
   *   The AWS service to put log inside cloudwatch.
   * @param \Drupal\aws_cloudwatchlogs\Services\DescribeLogGroups $log_groups
   *   The service to get log group of Cloudwatch.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\aws_cloudwatchlogs\Services\DescribeLogStreams $log_streams
   *   The service to get log stream of Cloudwatch.
   */
  public function __construct(PutLogEvents $put_log, DescribeLogGroups $log_groups, EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $config_factory, DescribeLogStreams $log_streams) {
    $this->putLog = $put_log;
    $this->logGroups = $log_groups;
    $this->eventDispatcher = $event_dispatcher;
    $this->configFactory = $config_factory;
    $this->logStreams = $log_streams;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    unset($context['backtrace'], $context['exception']);
    $log_level = isset(static::LOG_LEVELS[$level]) ? strtoupper(static::LOG_LEVELS[$level]) : strtoupper(LogLevel::INFO);
    $this->sendAws($log_level, $message, $context);
  }

  /**
   * Send log to aws cloudwatch.
   *
   * @param string $level
   *   The severity level of the message.
   * @param string $message
   *   The message.
   * @param array $context
   *   Associative array providing context.
   *
   * @return bool
   */
  private function sendAws(string $level, string $message, array $context = []) {
    $config = $this->configFactory->get(AwsCloudwatchLogSettingsForm::SETTINGS);
    $send_log = $config->get('send_log');
    $verify_group_stream = $config->get('verify_group_stream_before_send');
    if (empty($send_log['enable'])) {
      return FALSE;
    }
    $aws_client = $this->putLog->getClient();
    if (empty($aws_client)) {
      return FALSE;
    }
    // Format the original message.
    // Remove keys without a placeholder prefix (no longer supported).
    // @see Drupal\Component\Render\FormattableMarkup::placeholderFormat()
    $arguments = array_filter(
      $context,
      function ($key) {
        return in_array($key[0], ['@', ':', '%']);
      },
      ARRAY_FILTER_USE_KEY
    );
    $message = (string) (new FormattableMarkup($message, $arguments));

    // Wrap the message with metadata.
    $fmt = $send_log['message_format'] ?? static::DEFAULT_FMT;
    $message_formatted = new FormattableMarkup($fmt, [
      '@timestamp'   => $context['timestamp'],
      '@severity'    => $level,
      '@type'        => $context['channel'],
      '@message'     => strip_tags($message),
      '@uid'         => $context['uid'],
      '@request_uri' => $context['request_uri'],
      '@referer'     => $context['referer'],
      '@ip'          => $context['ip'],
      '@link'        => strip_tags($context['link'] ?? ''),
      '@date'        => date('Y-m-d\TH:i:s', $context['timestamp']),
    ]);
    $message_formatted = (string) $message_formatted;
    $log_group_options = [];

    if ($verify_group_stream) {
      // Options for log groups.
      $logGroupsList = $this->logGroups->getResult($aws_client);
      if ($logGroupsList) {
        $log_group_options = $this->logGroups->getLogGroupNameOnly($logGroupsList);
      }
    }
    $log_group = $send_log['log_group'] ?? '';
    $log_stream = $send_log['log_stream'] ?? '';
    $event = new AwsCloudwatchLogsSendEvent($message_formatted, $log_group, $log_stream, $context);
    $this->eventDispatcher->dispatch($event, AwsCloudwatchLogsSendEvent::SEND_LOGS);
    if ($verify_group_stream) {
      // Invalid log group.
      if (empty($log_group_options[$event->getLogGroup()])) {
        return FALSE;
      }
      $logStreams = $this->logStreams->getResult($aws_client, $log_group);
      $log_streams_options = $this->logStreams->getNamesOnly($logStreams);
      // Invaid log stream.
      if (empty($log_streams_options[$event->getLogStream()])) {
        return FALSE;
      }
    }
    $this->putLog->putLog(
      $aws_client,
      $event->getMessage(),
      $event->getLogGroup(),
      $event->getLogStream()
    );
    return TRUE;
  }

}
