<?php

namespace Drupal\aws_cloudwatchlogs\Form;

use Drupal\aws_cloudwatchlogs\Logger\Log;
use Drupal\aws_cloudwatchlogs\Services\DescribeLogGroups;
use Drupal\aws_cloudwatchlogs\Services\DescribeLogStreams;
use Drupal\aws_cloudwatchlogs\Services\GetLogEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure AWS Cloudwatch logs settings for this site.
 */
class AwsCloudwatchLogSettingsForm extends ConfigFormBase {

  /**
   * Stores the object of AWS Cloudwatch logs.
   *
   * @var \Drupal\aws_cloudwatchlogs\Services\GetLogEvents
   */
  protected $logEvents;


  /**
   * Object for service to Describe log groups.
   *
   * @var \Drupal\aws_cloudwatchlogs\Services\DescribeLogGroups
   */
  protected $logGroups;

  /**
   * Object for service to Describe log stream.
   *
   * @var \Drupal\aws_cloudwatchlogs\Services\DescribeLogStreams
   */
  protected $logStreams;

  /**
   * Constructs a AWS Cloudwatch Settings Form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\aws_cloudwatchlogs\Services\DescribeLogGroups $logGroups
   *   The log groups service.
   * @param \Drupal\aws_cloudwatchlogs\Services\GetLogEvents $getLogEvents
   *   The log events service.
   * @param \Drupal\aws_cloudwatchlogs\Services\DescribeLogStreams $logStreams
   *   The log streams service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DescribeLogGroups $logGroups, GetLogEvents $getLogEvents, DescribeLogStreams $logStreams) {
    parent::__construct($config_factory);
    $this->logGroups = $logGroups;
    $this->logEvents = $getLogEvents;
    $this->logStreams = $logStreams;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('aws_cloudwatchlogs.describe_log_groups'),
      $container->get('aws_cloudwatchlogs.get_log_events'),
      $container->get('aws_cloudwatchlogs.describe_log_streams')
    );
  }

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'aws_cloudwatchlogs.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_cloudwatchlogs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['basic_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Basic Settings'),
      '#open' => TRUE,
    ];

    $form['basic_settings']['region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#description' => $this->t('Region where your application is hosted in AWS.
        <p> For example: ap-south-1 for Asia Pacific (Mumbai) region </p>
        '),
      '#default_value' => $config->get('region'),
    ];

    $form['basic_settings']['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#description' => $this->t('Version for which connection is needed.
        Leave to latest if not known.'),
      '#default_value' => $config->get('version'),
    ];

    $form['authentication'] = [
      '#type' => 'details',
      '#title' => $this->t('Cloudwatch Authentication'),
      '#description' => $this->t('The access key and secret of the IAM user with apt
        roles/permissions assign for AWS Cloudwatch connection.'),
      '#open' => TRUE,
    ];

    $form['authentication']['access_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('AWS Access Key'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $config->get('access_key'),
    ];

    $form['authentication']['secret'] = [
      '#type' => 'key_select',
      '#title' => $this->t('AWS Secret Key'),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $config->get('secret'),
    ];

    $this->generateSendLog($form, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Generate select log group and log stream.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return false|void
   */
  private function generateSendLog(array &$form, FormStateInterface $form_state) {
    $client = $this->logEvents->getClient();
    if (empty($client)) {
      return FALSE;
    }
    $form['send_log'] = [
      '#type' => 'details',
      '#title' => $this->t('Send log'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $config = $this->config(static::SETTINGS);
    $send_log = $config->get('send_log');
    $form['send_log']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send log to AWS'),
      '#description' => $this->t('If check, all drupal logs will be send to AWS Cloudwatch'),
      '#default_value' => $send_log['enable'] ?? FALSE,
    ];

    $form['send_log']['message_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message Format'),
      '#description' => $this->t('Define the format of log messages. Available placeholders: @severity, @type, @date, @message, @uid, @request_uri, @referer, @ip, @link, @timestamp'),
      '#default_value' => $send_log['message_format'] ?? Log::DEFAULT_FMT,
      '#states' => [
        'visible' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
        'required' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $logGroupOptions = $logStreamsOptions = [];
    // Options for log groups.
    $logGroupsList = $this->logGroups->getResult($client);
    if ($logGroupsList) {
      $logGroupOptions = $this->logGroups->getLogGroupNameOnly($logGroupsList);
    }
    $send_value = $form_state->getValue('send_log');
    $log_group = $default_send_log['log_group'] ?? '';
    if (!empty($send_value['log_group'])) {
      $log_group = $send_value['log_group'];
    }
    if (empty($log_group)) {
      $log_group = reset($logGroupOptions);
    }

    if (!empty($log_group)) {
      // Fetch matching log streams.
      $client = $this->logStreams->getClient();
      $logStreams = $this->logStreams->getResult($client, $log_group);
      $logStreamsOptions = $this->logStreams->getNamesOnly($logStreams);
    }

    $form['send_log']['log_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Log Group Name'),
      '#description' => $this->t('The name of the log group.'),
      '#options' => $logGroupOptions,
      '#default_value' => $log_group,
      '#ajax' => [
        'callback' => '::getMatchingLogStreams',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'log-streams-outer-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Updating Log Streams...'),
        ],
      ],
      '#weight' => 1,
      '#states' => [
        'visible' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
        'required' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $log_stream = $send_log['log_stream'] ?? '';
    if (!empty($send_value['log_stream'])) {
      $log_stream = $send_value['log_stream'];
    }
    $form['send_log']['log_stream'] = [
      '#type' => 'select',
      '#title' => $this->t('Log Stream Name'),
      '#description' => $this->t('The name of the log stream under respective log group.'),
      '#options' => $logStreamsOptions,
      '#default_value' => $log_stream,
      '#prefix' => '<div id="log-streams-outer-wrapper">',
      '#suffix' => '</div>',
      '#multiple' => FALSE,
      '#weight' => 2,
      '#states' => [
        'visible' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
        'required' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['send_log']['verify_group_stream_before_send'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verify group/stream before send'),
      '#description' => $this->t('If checked, verify the configured log group and stream before sending a log message.'),
      '#default_value' => $send_log['verify_group_stream_before_send'] ?? FALSE,
      '#weight' => 2,
      '#states' => [
        'visible' => [
          'input[name="send_log[enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }

  /**
   * Ajax handler for returning matching log streams.
   */
  public function getMatchingLogStreams(array &$form, FormStateInterface $form_state) {
    return $form['send_log']['log_stream'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $config->set('region', $form_state->getValue('region'));
    $config->set('version', $form_state->getValue('version'));
    $config->set('access_key', $form_state->getValue('access_key'));
    $config->set('secret', $form_state->getValue('secret'));
    $config->set('send_log', $form_state->getValue('send_log'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
